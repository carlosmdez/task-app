import React, { Component } from 'react'

export default class Navigation extends Component {
  render() {
    return (
      <nav className='light-blue darken-4'>
        <div className='container'>
          <a className='brand-logo' href='/'>Tasks manager</a>
        </div>
      </nav>
    )
  }
}
