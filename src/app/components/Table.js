import React, { Component } from 'react'

export default class Table extends Component {
  render() {
    const tasksData = this.props.tasks.map(task => {
      return (
        <tr key={task._id}>
          <td>{task.title}</td>
          <td>{task.description}</td>
          <td>
            <button onClick={() => this.props.editTask(task._id)} className="btn light-blue darken-4" style={{ margin: 4 }}>
              <i className="material-icons">edit</i>
            </button>
            <button onClick={() => this.props.deleteTask(task._id)} className="btn light-blue darken-4" style={{ margin: 4 }}>
              <i className="material-icons">delete</i>
            </button>
          </td>
        </tr>
      )
    })
    return (
      <div className="col s7">
        <table>
          <thead>
            <tr>
              <th>Title</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>{tasksData}</tbody>
        </table>
      </div>
    )
  }
}
