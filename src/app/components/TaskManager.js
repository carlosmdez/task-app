import React, { Component } from 'react'
import Table from './Table';

export default class TaskManager extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col s5">
            <div className="card">
              <div className="card-content">
                <form onSubmit={this.props.onAddTask}>
                  <div className="row">
                    <div className="input-field col s12">
                      <input
                        name="title"
                        onChange={this.props.changed}
                        value={this.props.title}
                        type="text"
                        placeholder="Task Title"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="input-field col s12">
                      <textarea
                        name="description"
                        onChange={this.props.changed}
                        value={this.props.description}
                        className="materialize-textarea"
                        placeholder="Task Description"
                      />
                    </div>
                  </div>
                  <button type="submit" className="btn light-blue darken-4">
                    Send
                  </button>
                </form>
              </div>
            </div>
          </div>
          <Table editTask={this.props.editTask} deleteTask={this.props.deleteTask} tasks={this.props.tasks}/>
        </div>
      </div>
    )
  }
}
