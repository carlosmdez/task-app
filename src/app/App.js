import React, { Component } from 'react'
import Navigation from './components/Navigation'
import TaskManager from './components/TaskManager'

class App extends Component {
  constructor() {
    super()
    this.state = {
      title: '',
      description: '',
      tasks: [],
      _id: ''
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.onAddTaskHandler = this.onAddTaskHandler.bind(this)
    this.fetchTasks = this.fetchTasks.bind(this)
  }

  componentDidMount() {
    this.fetchTasks()
  }

  onAddTaskHandler = event => {
    if (this.state._id) {
      fetch(`/api/tasks/${this.state._id}`, {
        method: 'PUT',
        body: JSON.stringify(this.state),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          M.toast({ html: 'Task Updated' })
          this.setState({ title: '', description: '', _id: '' })
          this.fetchTasks()
        })
        .catch(err => console.log(err))
    } else {
      fetch('/api/tasks', {
        method: 'POST',
        body: JSON.stringify(this.state),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          M.toast({ html: 'Task Saved' })
          this.setState({ title: '', description: '' })
          this.fetchTasks()
        })
        .catch(err => console.log(err))
    }
    event.preventDefault()
  }

  fetchTasks = () => {
    fetch('api/tasks')
      .then(res => res.json())
      .then(data => {
        this.setState({ tasks: data })
        console.log(this.state.tasks)
      })
      .catch(err => console.log(err))
  }

  editTaskHandler = id => {
    fetch(`/api/tasks/${id}`)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        const { title, description, _id } = data
        this.setState({ title, description, _id })
      })
      .catch(err => console.log(err))
  }

  deleteTaskHandler = id => {
    if (confirm('Are you sure you want to delete this task?')) {
      console.log('deleting', id)
      fetch(`/api/tasks/${id}`, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          M.toast({ html: 'Task Deleted' })
          this.fetchTasks()
        })
        .catch(err => console.log(err))
    }
  }

  onChangeText = event => {
    const { name, value } = event.target
    this.setState({ [name]: value })
  }

  render() {
    return (
      <div>
        <Navigation />
        <TaskManager
          title={this.state.title}
          description={this.state.description}
          changed={this.onChangeText}
          onAddTask={this.onAddTaskHandler}
          tasks={this.state.tasks}
          deleteTask={this.deleteTaskHandler}
          editTask={this.editTaskHandler}
        />
      </div>
    )
  }
}

export default App
